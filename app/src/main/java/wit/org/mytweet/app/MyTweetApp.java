package wit.org.mytweet.app;

import static wit.org.android.helpers.LogHelpers.info;

import wit.org.mytweet.models.Portfolio;
import wit.org.mytweet.models.PortfolioSerializer;

import android.app.Application;


public class MyTweetApp extends Application
{
    private static final String FILENAME = "portfolio.json";

    public Portfolio portfolio;

    @Override
    public void onCreate()
    {
        super.onCreate();
        PortfolioSerializer serializer = new PortfolioSerializer(this, FILENAME);
        portfolio = new Portfolio(serializer);

        info(this, "MyTweet app launched");
    }
}
