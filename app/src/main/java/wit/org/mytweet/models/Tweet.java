package wit.org.mytweet.models;

import java.text.DateFormat;
import java.util.Date;
import java.util.UUID;

import org.json.JSONException;
import org.json.JSONObject;

import wit.org.mytweet.R;
import android.content.Context;

public class Tweet
{
    public UUID id;
    public String messageTweet;
    public Date    date;
    public String  contact;

    private static final String JSON_ID              = "id"            ;
    private static final String JSON_messageTWEET    = "messageTweet"   ;
    private static final String JSON_DATE            = "date"          ;
    private static final String JSON_CONTACT         = "contact";

    public Tweet()
    {
        id = UUID.randomUUID();
        messageTweet = "";
        date         = new Date();
        contact      = " no contact";
    }

    public Tweet(JSONObject json) throws JSONException
    {
        id = UUID.fromString(json.getString(JSON_ID));
        messageTweet  = json.getString(JSON_messageTWEET);
        date          = new Date(json.getLong(JSON_DATE));
        contact       = json.getString(JSON_CONTACT);
    }

    public JSONObject toJSON() throws JSONException
    {
        JSONObject json = new JSONObject();
        json.put(JSON_ID             , id.toString());
        json.put(JSON_messageTWEET   , messageTweet);
        json.put(JSON_DATE           , date.getTime());
        json.put(JSON_CONTACT        , contact);
        return json;
    }

    public String getDateString()
    {
        return "Tweet Sent: " + DateFormat.getDateTimeInstance().format(date);
    }
// create tweetreport for email
    public String getTweetReport(Context context)
    {
        String dateFormat = "EEE, MMM dd";
        String dateString = android.text.format.DateFormat.format(dateFormat, date).toString();
        String selectedContact = contact;
        if (contact == null)
        {
            selectedContact = context.getString(R.string.tweet_report_no_selected_contact);
        }
        else
        {
            selectedContact = context.getString(R.string.tweet_report_selected_contact, contact);
        }
        String report =  "Message Tweet: " + messageTweet + " Date: " + dateString + selectedContact;
        return report;
    }
}
