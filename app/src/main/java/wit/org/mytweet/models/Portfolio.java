package wit.org.mytweet.models;

import static wit.org.android.helpers.LogHelpers.info;

import java.util.ArrayList;
import java.util.UUID;

import android.util.Log;

public class Portfolio
{
    public ArrayList<Tweet> tweets;
    private PortfolioSerializer serializer;

    public Portfolio(PortfolioSerializer serializer)
    {
        this.serializer = serializer;
        try
        {
            tweets = serializer.loadTweets();
        } catch (Exception e)
        {
            info(this, "Error loading tweets: " + e.getMessage());
            tweets = new ArrayList<Tweet>();
        }
    }
// save tweets to file
    // catch any errors
    public boolean saveTweets()
    {
        try {
            serializer.saveTweets(tweets);
            info(this, "Tweets saved to file");
            return true;
        } catch (Exception e)
        {
            info(this, "Error saving tweets: " + e.getMessage());
            return false;
        }
    }
// add tweets
    public void addTweet(Tweet tweet)
    {
        tweets.add(tweet);
    }
// get tweets
    public Tweet getTweet(UUID id)
    {
        Log.i(this.getClass().getSimpleName(), "UUID parameter id: " + id);

        for (Tweet twe : tweets)
        {
            if (id.equals(twe.id))
            {
                return twe;
            }
        }
        info(this, "failed to find tweet. returning first element array to avoid crash");
        return null;
    }
// deleteTweet method when called removes a tweet
    public void deleteTweet(Tweet c)
    {
        tweets.remove(c);
    }
}