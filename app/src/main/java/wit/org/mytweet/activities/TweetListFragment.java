package wit.org.mytweet.activities;

import java.util.ArrayList;
import wit.org.android.helpers.IntentHelper;
import wit.org.mytweet.R;
import wit.org.mytweet.app.MyTweetApp;
import wit.org.mytweet.models.Portfolio;
import wit.org.mytweet.models.Tweet;

import android.view.ActionMode;
import android.widget.AbsListView;
import android.widget.ListView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.TextView;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ListFragment;


public class TweetListFragment extends ListFragment implements AdapterView.OnItemClickListener, AbsListView.MultiChoiceModeListener
{
    private ArrayList<Tweet> tweets;
    private ListView listView;
    private Portfolio portfolio;
    private TweetAdapter adapter;

// OnCreate setTitle = time line
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        getActivity().setTitle(R.string.app_name_time_line);

        MyTweetApp app = (MyTweetApp) getActivity().getApplication();
        portfolio = app.portfolio;
        tweets = portfolio.tweets;
        adapter = new TweetAdapter(getActivity(), tweets);
        setListAdapter(adapter);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState)
    {
        View v = super.onCreateView(inflater, parent, savedInstanceState);
        getActivity().setTitle(R.string.app_name_time_line);
        listView = (ListView) v.findViewById(android.R.id.list);
        listView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE_MODAL);
        listView.setMultiChoiceModeListener(this);
        return v;
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id)
    {
        Tweet twe = ((TweetAdapter) getListAdapter()).getItem(position);
        Intent i = new Intent(getActivity(), MyTweetPagerActivity.class);
        i.putExtra(MyTweetFragment.EXTRA_TWEET_ID, twe.id);
        startActivityForResult(i, 0);
    }

    @Override
    public void onResume()
    {
        super.onResume();
        ((TweetAdapter) getListAdapter()).notifyDataSetChanged();
    }
// display tweet list menu
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater)
    {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.tweetlist, menu);
    }
    // onOptionsItemSelected when plus button is pressed create new tweet then go to mytweet page
    // when clear button is pressed remove all tweets.
    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        switch (item.getItemId())
        {
            case R.id.menu_item_new_tweet:
                Tweet tweet = new Tweet();
                portfolio.addTweet(tweet);
                Intent i = new Intent(getActivity(), MyTweetPagerActivity.class);
                i.putExtra(MyTweetFragment.EXTRA_TWEET_ID, tweet.id);
                startActivityForResult(i, 0);
                return true;

            case R.id.action_clear:
                adapter.clear();
                return true;

            default:
                return super.onOptionsItemSelected(item);

        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id)
    {
        Tweet tweet = adapter.getItem(position);
        IntentHelper.startActivityWithData(getActivity(), MyTweetPagerActivity.class, "TWEET_ID", tweet.id);
    }
    // show menu
    @Override
    public boolean onCreateActionMode(ActionMode actionMode, Menu menu)
    {

        MenuInflater inflater = actionMode.getMenuInflater();
        inflater.inflate(R.menu.tweet_list_context, menu);
        return true;
    }

    @Override
    public boolean onPrepareActionMode(ActionMode mode, Menu menu)
    {
        return false;
    }
    // onActionItemClicked when delete button is pressed delete tweet
    @Override
    public boolean onActionItemClicked(ActionMode mode, MenuItem menuItem)
    {
        switch (menuItem.getItemId())
        {
            case R.id.menu_item_delete_tweet:
                deleteTweet(mode);
                return true;
            default:
                return false;
        }
    }
// method to delete tweets
    private void deleteTweet(ActionMode actionMode)
    {
        for (int i = adapter.getCount() - 1; i >= 0; i--)
        {
            if (listView.isItemChecked(i))
            {
                portfolio.deleteTweet(adapter.getItem(i));
            }
        }
        actionMode.finish();
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onDestroyActionMode(ActionMode mode)
    {}

    @Override
    public void onItemCheckedStateChanged(ActionMode mode, int position, long id, boolean checked)
    {}

class TweetAdapter extends ArrayAdapter<Tweet>
{
    private Context context;

    public TweetAdapter(Context context, ArrayList<Tweet> tweets)
    {
        super(context, 0, tweets);
        this.context = context;
    }

    @SuppressLint("InflateParams")
    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null)
        {
            convertView = inflater.inflate(R.layout.list_item_tweet, null);
        }
        Tweet twe = getItem(position);

        TextView tweetTextView = (TextView) convertView.findViewById(R.id.tweet_list_item_tweet);
        tweetTextView.setText(twe.messageTweet);

        TextView dateTextView = (TextView) convertView.findViewById(R.id.tweet_list_item_date);
        dateTextView.setText(twe.getDateString());

        return convertView;
    }
}
}