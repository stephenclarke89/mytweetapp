package wit.org.mytweet.activities;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;

import wit.org.mytweet.R;

import wit.org.mytweet.app.MyTweetApp;
import wit.org.mytweet.models.Portfolio;
import wit.org.mytweet.models.Tweet;

import java.util.ArrayList;
import java.util.UUID;

import static wit.org.android.helpers.LogHelpers.info;


public class MyTweetPagerActivity extends FragmentActivity implements ViewPager.OnPageChangeListener
{
    private ViewPager viewPager;
    private ArrayList<Tweet> tweets;
    private Portfolio portfolio;
    private PagerAdapter pagerAdapter;

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        viewPager = new ViewPager(this);
        viewPager.setId(R.id.viewPager);
        setContentView(viewPager);
        setTweetList();
        pagerAdapter = new PagerAdapter(getSupportFragmentManager(),tweets);
        viewPager.setAdapter(pagerAdapter);
        viewPager.addOnPageChangeListener(this);
        setCurrentItem();
    }

    private void setTweetList()
    {
        MyTweetApp app = (MyTweetApp) getApplication();
        portfolio = app.portfolio;
        tweets = portfolio.tweets;
    }

    private void setCurrentItem()
    {
        UUID twe = (UUID) getIntent().getSerializableExtra(MyTweetFragment.EXTRA_TWEET_ID);
        for (int i = 0; i < tweets.size(); i++)
        {
            if (tweets.get(i).id.toString().equals(twe.toString()))
            {
                viewPager.setCurrentItem(i);
                break;
            }
        }
    }
// allow you to scroll between tweets
    @Override
    public void onPageScrolled(int arg0, float arg1, int arg2)
    {
        info(this, "onPageScrolled: arg0 "+arg0+" arg1 "+arg1+" arg2 "+arg2);
        Tweet tweet = tweets.get(arg0);
        if (tweet.messageTweet != null)
        {
            setTitle(tweet.messageTweet);
        }
    }

    @Override
    public void onPageSelected(int position)
    {

    }

    @Override
    public void onPageScrollStateChanged(int state)
    {

    }

    class PagerAdapter extends FragmentStatePagerAdapter
    {
        private ArrayList<Tweet>  tweets;

        public PagerAdapter(FragmentManager fm, ArrayList<Tweet> tweets)
        {
            super(fm);
            this.tweets = tweets;
        }

        @Override
        public int getCount()
        {
            return tweets.size();
        }

        @Override
        public Fragment getItem(int pos)
        {
            Tweet tweet = tweets.get(pos);
            Bundle args = new Bundle();
            args.putSerializable(MyTweetFragment.EXTRA_TWEET_ID, tweet.id);
            MyTweetFragment fragment = new MyTweetFragment();
            fragment.setArguments(args);
            return fragment;
        }
    }
}
