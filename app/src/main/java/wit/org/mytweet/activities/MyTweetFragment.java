package wit.org.mytweet.activities;

import java.util.UUID;
import wit.org.android.helpers.ContactHelper;
import wit.org.mytweet.R;
import wit.org.mytweet.app.MyTweetApp;
import wit.org.mytweet.models.Portfolio;
import wit.org.mytweet.models.Tweet;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import static wit.org.android.helpers.IntentHelper.sendEmail;
import static wit.org.android.helpers.IntentHelper.navigateUp;
// MyTweetFragment extends Fragment and uses textWatcher to watch when text changes
// OnClickListener listens for a click event and then does something.
// Bring in the required TextView Button etc.
public class MyTweetFragment extends Fragment implements TextWatcher,
        OnClickListener
{
    public static   final String  EXTRA_TWEET_ID = "tweet.TWEET_ID";

    private static  final int     REQUEST_CONTACT = 1;

    private EditText messageTweet;
    private TextView textCount;
    private TextView date;
    private Button   contactButton;
    private Button   emailButton;
    private Button   tweetButton;

    private Tweet tweet;
    private Portfolio portfolio;

    // When the page is created set the title of the page to MyTweet.
    // Get saved Tweets.
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        getActivity().getActionBar().setTitle("MyTweet");

        UUID tweId = (UUID)getArguments().getSerializable(EXTRA_TWEET_ID);

        MyTweetApp app = (MyTweetApp) getActivity().getApplication();
        portfolio = app.portfolio;
        tweet = portfolio.getTweet(tweId);
    }
    // onCreateView fragment_tweet
    // addListeners to the view

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState)
    {
        super.onCreateView(inflater, parent, savedInstanceState);
        View v = inflater.inflate(R.layout.fragment_tweet, parent, false);

        getActivity().getActionBar().setDisplayHomeAsUpEnabled(true);
        addListeners(v);
        updateControls(tweet);

        return v;
    }
    // addListeners to Button EditText etc that need to be monitored.
    //
    private void addListeners(View v)
    {
        messageTweet  = (EditText) v.findViewById(R.id.messageTweet);
        textCount     = (TextView) v.findViewById(R.id.textCount);
        date          = (TextView) v.findViewById(R.id.date);
        contactButton = (Button)   v.findViewById(R.id.contactButton);
        emailButton   = (Button)   v.findViewById(R.id.emailButton);
        tweetButton   = (Button)   v.findViewById(R.id.tweetButton);


        messageTweet.addTextChangedListener(this);
        textCount.setOnClickListener(this);
        date.setOnClickListener(this);
        contactButton.setOnClickListener(this);
        emailButton.setOnClickListener(this);
        tweetButton.setOnClickListener(this);
    }
    // updateControls updates Tweet to have text in messageTweet to tweet
    // and date to string to tweet.
    public void updateControls(Tweet tweet)
    {
        messageTweet.setText(tweet.messageTweet);
        date.setText(tweet.getDateString());
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        // It returns you back to the parent page which you have indicated in the Manifest
        // when the up button is pressed.
        switch (item.getItemId())
        {
            case android.R.id.home:  navigateUp(getActivity());
                return true;
            default: return super.onOptionsItemSelected(item);
        }
    }
    // When the app goes onPause the portfolio method saveTweets saves the tweets
    public void onPause()
    {
        super.onPause();
        portfolio.saveTweets();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        if (resultCode != Activity.RESULT_OK)
        {
            return;
        }
        else
        if (requestCode == REQUEST_CONTACT)
        {
            String name = ContactHelper.getContact(getActivity(), data);
            tweet.contact = name;
            contactButton.setText(name);
        }
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after)
    {}

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count)
    {}
    // textCount to display string of value 140 - the length of text entered in editText plus
    // Characters Remaining
    // If text in editText is greater than or = 140 change the colour of the text within text count red
    // and display toast Max Characters Reached!
    // If the editText length is less than 140 make text in textCount black.
    @Override
        public void afterTextChanged(Editable s)
        {
           textCount.setText(String.valueOf(140 - s.length()+" Characters Remaining"));

                if (s.length()>=140)
                {
                    textCount.setTextColor(Color.RED);
                 Toast.makeText(getActivity(),"Max Characters Reached!", Toast.LENGTH_SHORT).show();
                }
                if (s.length()<140)
                {
                    textCount.setTextColor(Color.BLACK);
                }
        }
    // When contactButton is pressed go to contacts
    // When contact selected change contactButton to name of contact
    // When emailButton is pressed go to email
    // When tweetButton is pressed and messageTweet is blank create toast You Cannot Send Blank Tweets
    // When tweetButton is pressed and messageTweet has characters create tweet.
    @Override
    public void onClick(View v)
    {
        switch (v.getId())
        {
            case R.id.contactButton                 : Intent i = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
                startActivityForResult(i, REQUEST_CONTACT);
                if (tweet.contact != null)
                {
                    contactButton.setText("Contact: "+tweet.contact);
                }
                break;

            case R.id.emailButton : sendEmail(getActivity(), "", getString(R.string.tweet_report_subject), tweet.getTweetReport(getActivity()));
                break;

            case R.id.tweetButton:
                if (messageTweet.getText().length()==0)
                {
                    Toast.makeText(getActivity(),"You Cannot Send Blank Tweets", Toast.LENGTH_SHORT).show();
                }
           else {
                    String tweetString = messageTweet.getText().toString();
                    Tweet createTweet = new Tweet();
                    createTweet.messageTweet = tweetString;
                    portfolio.addTweet(createTweet);
                    Toast.makeText(getActivity(),"Message Sent", Toast.LENGTH_SHORT).show();
                    break;
                }
        }
    }

}