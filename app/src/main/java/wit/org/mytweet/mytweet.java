package wit.org.mytweet;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.TimeZone;
import java.util.Locale;

public class mytweet extends AppCompatActivity implements TextWatcher
{
    private EditText messageTweet;
    private TextView textCount;
    private TextView date;
    Calendar cal = Calendar.getInstance(TimeZone.getDefault());
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mytweet);
        messageTweet = (EditText) findViewById(R.id.messageTweet);
        messageTweet.addTextChangedListener(this);
        textCount = (TextView) findViewById(R.id.textCount);
        date = (TextView) findViewById(R.id.date);

        SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy, hh:mm:ss", Locale.getDefault());
        date.setText(sdf.format(cal.getTime()));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_mytweet, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings)
        {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after)
    {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count)
    {

    }

    @Override
    public void afterTextChanged(Editable s)
    {
        textCount.setText(String.valueOf(140 - s.length()+" Characters Remaining"));

        if (s.length()>=140)
        {
            textCount.setTextColor(Color.RED);
            Toast toast = Toast.makeText(this, "Max Characters Reached!", Toast.LENGTH_SHORT);
            toast.show();
        }
        if (s.length()<140)
        {
            textCount.setTextColor(Color.BLACK);
        }
    }
}
